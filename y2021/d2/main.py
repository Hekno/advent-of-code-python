def solution_1(input_file):
    depth = 0
    horizontal_position = 0

    for line in input_file.readlines():
        words = line.split()
        if words[0] == "up":
            depth -= int(words[1])
        elif words[0] == "down":
            depth += int(words[1])
        elif words[0] == "forward":
            horizontal_position += int(words[1])

    print("depth:", depth, "\nhorizontal_position:", horizontal_position)
    print("answer:", depth * horizontal_position)


def solution_2(input_file):
    aim = 0
    depth = 0
    horizontal_position = 0

    for line in input_file.readlines():
        words = line.split()
        if words[0] == "up":
            aim -= int(words[1])
        elif words[0] == "down":
            aim += int(words[1])
        elif words[0] == "forward":
            horizontal_position += int(words[1])
            depth += aim * int(words[1])

    print("depth:", depth,
          "\nhorizontal_position:", horizontal_position,
          "\naim:", aim)

    print("answer:", depth * horizontal_position)


if __name__ == "__main__":
    solution_2(open("input.txt", "rt"))
