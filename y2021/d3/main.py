def solution_1(input_file):

    counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    lines = input_file.readlines()
    for string in lines:
        for i in range(len(string.strip())):
            print(i)
            if int(string[i]) == 0:
                counter[i] -= 1
            elif int(string[i]) == 1:
                counter[i] += 1
    
    gamma_bits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    epsilon_bits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    for i in range(len(counter)):
        if counter[i] > 0:
            gamma_bits[i] = 1
            epsilon_bits[i] = 0
        elif counter[i] < 0:
            gamma_bits[i] = 0
            epsilon_bits[i] = 1

    gamma_decimal = int(''.join(map(str, gamma_bits)), 2)
    epsilon_decimal = int(''.join(map(str, epsilon_bits)), 2)

    print("gamma:", gamma_decimal)
    print("epsilon:", epsilon_decimal)
    print("battery usage:", gamma_decimal * epsilon_decimal)

def solution_2():
    pass


if __name__ == "__main__":
    solution_1(open("input.txt"))