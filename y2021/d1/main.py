def solution_1(input_file):
    previous_scan = 0
    increase_counter = 0

    for line in input_file.readlines():
        if int(line.strip()) > previous_scan and previous_scan != 0:
            increase_counter += 1
        previous_scan = int(line)
    print("Solution 1:", increase_counter)


def solution_2(input_file):
    window_width = 3
    previous_window = 0
    increase_counter = 0
    lines = input_file.readlines()
    for i in range(len(lines) - (window_width - 1)):
        window = 0
        window_slice = slice(i, i + window_width)

        for line in lines[window_slice]:
            window += int(line.strip())
        print("i:", i,
              "\nslice:", window_slice,
              "\nwindow:", window,
              "\nprevious_window:", previous_window,
              "\nincreased:", window > previous_window)

        if window > previous_window and previous_window != 0:
            increase_counter += 1

        previous_window = window
    print(increase_counter)


if __name__ == "__main__":

    solution_2(open("input.txt", "rt"))
